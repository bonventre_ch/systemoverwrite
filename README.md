# SystemOverwrite

This iOS Tweak let's you replace (almost) any String with a custom one. Configured from settings.app
This is a beta Version and not everything is working as intended. Please report if you find something that isn't changing.

Please report bugs to me, preferably via an issue here: https://gitlab.com/LeroyB_/systemoverwrite/issues/new
All my project are open sourced and are meant to be for learning. Please view the license file for more info.